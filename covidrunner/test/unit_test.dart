import 'package:covidrunner/homepage.dart';
import 'package:covidrunner/covid.dart';
import 'package:test/test.dart';

void main() {
  test('Test returns 1', () {
    final covid = MyCovid();
    expect(covid.testthis(), 1);
  });
}