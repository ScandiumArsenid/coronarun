import "dart:async";

import "package:covidrunner/barriers.dart";
import 'package:flutter/material.dart';
import "package:covidrunner/covid.dart";

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<HomePage> {
// bird variables
  static double covidY = 0;
  double intitialPos = covidY;
  double height = 0;
  double time = 0;
  double gravity = -3; //Anziehungskraft
  double velocity = 2.8; //Sprungkraft
  double barrierXtwo = barrierXone + 1.5;
  static double barrierXone = 1;

  // game settings
  bool gameHasStarted = false;

  void startGame() {
    gameHasStarted = true;
    Timer.periodic(const Duration(milliseconds: 60), (timer) {
      //Sprung
      height = gravity * time * time + velocity * time;

      setState(() {
        covidY = intitialPos - height;

        if (barrierXone < -1.1) {
          barrierXone += 2.2;
        } else {
          barrierXone -= 0.01;
        }

        //barrierXone -= 0.03;
        //barrierXtwo -= 0.03;
      });

      setState(() {

        if (barrierXtwo < -1.1) {
          barrierXtwo += 2.2;
        } else {
          barrierXtwo -= 0.01;
        }

        //barrierXone -= 0.03;
        //barrierXtwo -= 0.03;
      });

      //Check  if corona ist tot
      if (covidIsDead()) {
        timer.cancel();
        gameHasStarted = false;
        _showDialog();
      }

      //Die Zeit geht weiter
      time += 0.1;
    });
  }

  void resetGame() {
    Navigator.pop(context);
    setState(() {
      covidY = 0;
      gameHasStarted = false;
      time = 0;
      intitialPos = covidY;
    });
  }

  void _showDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.brown,
            title: Center(
              child: Text(
                "G A M E  O V E R",
                style: TextStyle(color: Colors.white),
              ),
            ),
            actions: [
              GestureDetector(
                onTap: resetGame,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Container(
                    padding: EdgeInsets.all(7),
                    color: Colors.white,
                    child: Text(
                      'PLAY AGAIN',
                      style: TextStyle(color: Colors.brown),
                    ),
                  ),
                ),
              )
            ],
          );
        });
  }

  void jump() {
    setState(() {
      time = 0;
      intitialPos = covidY;
    });
  }

  bool covidIsDead() {
    //Check ob das Objekt(Corona) die Decke oder den Boden Berrührt oder nicht
    if (covidY < -1 || covidY > 1  || covidY==barrierXtwo  || covidY==barrierXone) {
      return true;
    }

    //Check if corona hits barriers

    return false;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: gameHasStarted ? jump : startGame,
      child: Scaffold(
          body: Column(children: [
        Expanded(
          flex: 3,
          child: Container(
            color: const Color.fromARGB(255, 124, 214, 255),
            child: Center(
                child: Stack(
              children: [
                MyCovid(
                  covidY: covidY,
                ),
                Container(
                  alignment: Alignment(
                    0,
                    -0.5,
                  ),
                  child: Text(
                    gameHasStarted ? '' : 'T A P  T O  P L A Y',
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
                AnimatedContainer(
                    alignment: Alignment(barrierXone, 1.1),
                    duration: Duration(milliseconds: 0),
                    child: MyBarrier(
                      size: 200.0,
                    )),
                AnimatedContainer(
                    alignment: Alignment(barrierXone, -1.1),
                    duration: Duration(milliseconds: 0),
                    child: MyBarrier(
                      size: 200.0,
                    )),
                AnimatedContainer(
                    alignment: Alignment(barrierXtwo, 1.1),
                    duration: Duration(milliseconds: 0),
                    child: MyBarrier(
                      size: 150.0,
                    )),
                AnimatedContainer(
                    alignment: Alignment(barrierXtwo, -1.1),
                    duration: Duration(milliseconds: 0),
                    child: MyBarrier(
                      size: 250.0,
                    ))
              ],
            )),
          ),
        ),
        Expanded(child: Container(color: Colors.brown))
      ])),
    );
  }
}
